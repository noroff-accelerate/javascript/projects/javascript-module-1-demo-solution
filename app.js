document.getElementById('coffeeOrderForm').addEventListener('submit', handleFormSubmission);
const finalPriceDisplay = document.getElementById('finalPrice')
const coffeeOptions = document.getElementById('coffeeType')
const quantityOrdered = document.getElementById('quantity')
const priceField = document.getElementById('price')
const orderList = document.getElementById('liOrders')
const coffeeTypeSelect = document.getElementById('coffeeType');
const coffeeSelectedImg = document.getElementById('img-selected-coffee');
coffeeOptions.addEventListener('change',handleCoffeeSelected)

let finalTotal = 0
let coffeeOptionsData = []
getCoffeeData()

function handleFormSubmission(event) {
    event.preventDefault(); // Prevents the default form submission behavior (page refresh)
        // Calculate the total price
        let totalPrice = quantityOrdered.value * priceField.value
        console.log(totalPrice)
        // Check if the quantity is higher than or equal to 5
        if (quantityOrdered.value >= 5) {

            // Apply the discount if true
            totalPrice = totalPrice - (totalPrice * 0.10)
        }

        // Add the order to list
        const newOrder = document.createElement('li')
        newOrder.innerText = `Order Placed: The total price for the ${quantityOrdered.value} ${coffeeOptionsData[coffeeOptions.selectedIndex].type}/s is €${totalPrice}`
        const newOrderImg = document.createElement('img')
        newOrderImg.src = coffeeOptionsData[coffeeOptions.selectedIndex].image
        newOrderImg.alt = coffeeOptionsData[coffeeOptions.selectedIndex].type
        newOrder.appendChild(newOrderImg)
        orderList.appendChild(newOrder)

        quantity.value = 0
        finalTotal += totalPrice
        finalPriceDisplay.innerText = `Final Price: €${finalTotal}`
        
}

async function getCoffeeData() {
    const response = await fetch('http://localhost:3000/coffee')
    const coffeeData = await response.json()
    coffeeOptionsData = coffeeData

    loadCoffeeOptions(coffeeData)
}

function loadCoffeeOptions(coffeeData) {
    coffeeData.forEach(coffee => {
        const newCoffeeOption = document.createElement('option')
        newCoffeeOption.value = coffee.type
        newCoffeeOption.innerText = coffee.type
        coffeeOptions.appendChild(newCoffeeOption)
    });
    handleCoffeeSelected()
}

function handleCoffeeSelected(){
    
    priceField.value = coffeeOptionsData[coffeeOptions.selectedIndex].price
    coffeeSelectedImg.src = coffeeOptionsData[coffeeOptions.selectedIndex].image
}

